$(document).ready(function () {
    'use strict';
    // ======== Header Icon Toggle ========
    $('header nav .brand .icon-toggle').click(function () {
        $('header .menu').addClass('active');
    });
    // ======== Header Icon Exit ========
    $('header .menu .exit').click(function () {
        $('header .menu').removeClass('active');
    });
    // ======== Smooth Scroll ========
    $('.smoothscroll').on('click', function (e) {
        e.preventDefault();
        var target = this.hash,
            $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 800, 'swing', function () {
            window.location.hash = target;
        });


    });
    $(window).scroll(function () {
        $('.BlockID').each(function () {
            if ($(window).scrollTop() > $(this).offset().top - 1) {
                var BlockID = $(this).attr('id');
                $('header .menu ul li a').removeClass('active');
                $('header .menu ul li a[data-scroll="' + BlockID + '"]').addClass('active');
            }
        });
    });

    // ======== Portfolio Item ========
    $('.portfolio button.more-item').click(function () {
        $('.portfolio .item-none').slideDown();
        $(this).fadeOut();
    });
    // ======== Testimonials Owl Carousel ========
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 1,
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true
    });
    // ======== Go To Top ========
    $(window).scroll(function () {
        if ($(window).scrollTop() > $('header').height()) {
            $('.go-to-top').fadeIn();
        } else {
            $('.go-to-top').fadeOut();
        }
    });
    // ======== Loading ========
    $(window).on('load', function () {
        $('.loading').fadeOut();
    });
    // ======== AOS Js ========
    AOS.init();
});